// IMPORTANT: Update this header if any change and put its contents to
// https://gitlab.com/inkscape/devel/chat-utils
// File in repo: RocketChat/RocketChatIntegrations/Rocket2IRCinkscape_user.txt
// OUTGOING
// Event Trigger : Message Sent
// Enabled : true
// Trigger Words : ""
// URLs : http://alpha.inkscape.org:9753
// Impersonate User : False
// Name : Rocket2IRC (#inkscape)
// Channel : #inkscape_user
// Post as : rocket.cat
// Alias : ""
// Avatar URL : ""
// Emoji : ""
// Token : XLxuSjpZS3ra2AQqKZTSR3My
// Script Enabled : True

class Script {
  /**
   * @params {object} request
   */
  prepare_outgoing_request({ request }) {
	if (request.data.user_name == "inkchatbot")  {
     	return false; 
    }
    return {
        url: request.url + "?json=" + encodeURIComponent('{"channel":"#inkscape","token":"XLxuSjpZS3ra2AQqKZTSR3My","user":"' + request.data.user_name+'","message_id": "'+request.data.message_id+'"}'),
        headers: request.headers,
        parseUrls: false,
        method: 'POST'
    };
  }
  /**
   * @params {object} request, response
   */
  process_outgoing_response({ request, response }) {
      return false;
  } 
}