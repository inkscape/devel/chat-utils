#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os, sys, socket, multiprocessing, urllib , shlex, json, cgi, requests, re, datetime, time, ssl, traceback, subprocess, logging
from http.server import HTTPServer, BaseHTTPRequestHandler
import logging.handlers as handlers
from pathlib import Path

RocketIRCBotDIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(RocketIRCBotDIR))
from inkchatbotConfig  import *
ircsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ircsock.connect((server, ircport))
ircsock.settimeout(None)

# logging
logger = logging.getLogger("inkchatbot")
logfile = RocketIRCBotDIR +"/" + "inkchatbot.log"
logging.basicConfig(filename=logfile, level=logging.DEBUG, 
    format='%(asctime)s - %(levelname)s', datefmt='%y-%m-%d %H:%M:%S')
logHandler = handlers.RotatingFileHandler(logfile, maxBytes=1024*1024*3, backupCount=1)
logHandler.setLevel(logging.DEBUG)
logger.addHandler(logHandler)

def inkexception(e,msg):
    logger.error(msg + " " + str(e), exc_info = True)
    #sys.exit("Exit")    
    raise

if Path("./rc.lock").is_file():
    os.remove("./rc.lock")
if Path("./rc2.lock").is_file():
    os.remove("./rc2.lock")
open("./" + rocketchatChannelId.get(channel1), 'w').close()
open("./" + rocketchatChannelId.get(channel2), 'w').close()
open("./" + rocketchatChannelId.get(channel3), 'w').close()
open("./" + rocketchatChannelId.get(channel9), 'w').close()

class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()

    def log_message(self, format, *args):
        pass

    def do_HEAD(self):
        self._set_headers()

    def do_GET(self):
        try:
            res = ""
            with open('users.json') as json_file:
                users = json.load(json_file)
            users[channel1]["nicks"] = sorted(users[channel1]["nicks"], key=str.lower)
            users[channel2]["nicks"] = sorted(users[channel2]["nicks"], key=str.lower)
            users[channel3]["nicks"] = sorted(users[channel3]["nicks"], key=str.lower)
            users[channel9]["nicks"] = sorted(users[channel9]["nicks"], key=str.lower)
            res = json.dumps(users)
            self.send_response(200)
            contentype = "application/json"
            if jsonp == True:
                contentype = "application/javascript"
                res = jsonpFunction + "(" + res+ ")"
            self.send_header("Content-type", contentype)
            self.send_header("Content-length", len(res))
            self.end_headers()
            self.wfile.write(bytes(res, 'UTF-8'))
        except Exception as err:
            inkexception(err, "error in do_get")

    def do_POST(self):
        try:
            self._set_headers()
            data = json.loads(f"[{urllib.parse.unquote(self.path.split('=')[1])}]")
            data = data[0] 
            messageid = data["message_id"]
            if messageid is not None and data["token"] == integrationToken:
                headers = {
                    'X-Auth-Token': rocketchatUserToken ,
                    'X-User-Id': rocketchatUserId,
                }
                params = (
                    ('msgId', messageid),
                )
                response = requests.get('https://chat.inkscape.org/api/v1/chat.getMessage', headers=headers, params=params)
                response.raise_for_status()
                finalmsg = response.json()
                user = data["user"]
                txt = "<" + '\x03' + ("%02d" % (hash(user) % 14 + 2)) + user + '\x03' + "> "
                takefinalmessage = finalmsg.get('message')
                if takefinalmessage is not None:
                    if takefinalmessage.get('editedAt') is not None:
                        if rocketedit == False:
                            return
                        txt = txt + "[Edited] "
                    if takefinalmessage.get('attachments') is not None:
                        for attach in takefinalmessage.get('attachments'):
                            if attach is not None and \
                            attach.get('description') is not None and \
                            attach.get('title_link') is not None and \
                            attach.get('title') is not None:
                                txt = txt + attach.get('description')
                                txt = txt + ", see on https://chat.inkscape.org/"
                                txt = txt + attach.get('title_link')
                                txt = txt + " (" + attach.get('title') + ") \r\n"
                    if takefinalmessage["msg"] is not None:
                        counter = 0
                        for line in re.split("\r\n",takefinalmessage["msg"]):
                            for linen in line.split("\n"):
                                for liner in linen.split("\r"):
                                    counter = counter + 1
                                    if counter < 10:
                                        txt = txt + liner + "\r\n"
                                    elif counter == 10:
                                        txt = txt + "https://chat.inkscape.org/channel/" + rocketchatChannelName.get(data["channel"]) + "?msg=" + messageid + "\r\n";
                    txt = re.sub(r'(^| )@(\w\w+)( |$|. )', r'\1@ \2 \3', txt, 20, re.MULTILINE)
                    for line in re.split("\r\n",txt):
                        for linen in re.split("\r\n",line):
                            for liner in linen.split("\r"):
                                sendmsg(liner, data["channel"])
        except Exception as err:
            logger.error(str(err))


def split_utf8(s , n):
    assert n >= 4
    start = 0
    lens = len(s)
    while start < lens:
        if lens - start <= n:
            yield s[start:]
            return
        end = start + n
        while '\x80' <= s[end] <= '\xBF':
            end -= 1
        assert end > start
        yield s[start:end]
        start = end

def reload():
    subprocess.call("export PYTHONHASHSEED=0 && bash ./inkchatbotCron.bash", shell=True)

def connect():
    try:
        ircsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        ircsock.connect((server, ircport))
        ircsock.settimeout(None)
        login()
    except Exception as err:
        inkexception(err, "error in connect")

def login():
    ircsock.send(bytes("USER "+ botnick +"  8 * :" + rocketBotName + "\r\n", 'UTF-8')) # user information
    time.sleep(30.0)
    ircsock.send(bytes("NICK "+ botnick +"\r\n", 'UTF-8'))
    joinchan(channel1)
    joinchan(channel2)
    joinchan(channel3)
    joinchan(channel9)

def joinchan(chan): # join channel(s).
    ircsock.send(bytes("JOIN "+ chan +"\r\n", 'UTF-8'))
    ircmsg = ""
    try:
        while ircmsg.find("End of /NAMES") == -1:
            ircmsg = ircsock.recv(2048).decode('UTF-8')
            ircmsg = ircmsg.strip('\r\n')
            if ircmsg != "":
                print(bytes(ircmsg, 'UTF-8'))
                if ircmsg.find("You have not registered") != -1:
                    ircsock.send(bytes("PRIVMSG NickServ :identify "+ password +"\r\n", 'UTF-8'))
                if ircmsg.find("Connection timed out") != -1:
                    time.sleep(30.0)
                    reload()
                if ircmsg.find("PING :") != -1:
                    ping()
                if ircmsg.find("rocketirc has disconnected") != -1:
                    time.sleep(30.0)
                    reload()
        ircsock.send(bytes("PRIVMSG NickServ :identify "+ password +"\r\n", 'UTF-8'))
    except Exception as err:
        inkexception(err, "error in join")

def ping():
    ircsock.send(bytes("PONG :pingis\r\n", 'UTF-8'))

def sendmsg(msg, channel):
    #FIXME we remove 70 from 510 chars to remove the initail message with server
    for msgsplit in split_utf8(msg , 440 - len(botnick + " PRIVMSG "+ channel +" :rn")):#510 with starting message
        time.sleep(0.33); #Prevent excess of flood
        ircsock.send(bytes("PRIVMSG "+ channel +" :"+ msgsplit.replace("https://chat.inkscape.org//","https://chat.inkscape.org/") +"\r\n", 'UTF-8'))

def toIRC():
    try:
        # Create server
        rocketchatUserId = ""
        rocketchatUserToken = ""
        currentdate = time.strftime("%x")
        httpd = HTTPServer(("", serverport), SimpleHTTPRequestHandler)
        if ssl == True:
            httpd.socket = ssl.wrap_socket (httpd.socket,
            keyfile = sslCertificateKey,
            certfile = sslCertificate, server_side=True)
        print("Serving on:" + str(serverport) + "...")
        httpd.serve_forever()
    except Exception as err:
        inkexception(err, "error in to rc")

def names(channel): # sends messages to the target.
    ircsock.send(bytes("NAMES "+ channel +"\r\n", 'UTF-8'))

def retoken():
    try:
        headers = {
            'Content-type' :'application/json; charset=UTF-8',
        }
        params = {'user': botnick,'password': password}
        try:
            response = requests.post("https://chat.inkscape.org/api/v1/login", headers=headers, json=params)
            response.raise_for_status()
        except requests.HTTPError as e:
            time.sleep(5)
            retoken()
            return
        autenticate = response.json()
        data = autenticate.get('data')
        if data is not None:
            file = open("./inkchatbotToken.py","w")
            file.write("rocketchatUserId = \"" + data.get('userId') + "\" \n")
            file.write("rocketchatUserToken = \"" + data.get('authToken') + "\" \n")
            file.close()
            rocketchatUserId = data.get('userId')
            rocketchatUserToken = data.get('authToken')
    except Exception as err:
        inkexception(err, "error in retoken")

def toRocketChat():
    while 1:
        try:
            ircmsg = ircsock.recv(2048).decode('UTF-8', "ignore")
            if ircmsg.find("PING :") != -1:
                ping()
                continue
            while Path("./rc2.lock").is_file():
                time.sleep(1)
            open("./rc.lock", 'a').close()
            idchann = rocketchatChannelId.get(channel9)
            if idchann and idchann != "":
                fname = "./" + idchann
                file = open(fname,"a")
                file.write(ircmsg)
                file.write(idchann + "-----")
                file.close()
            os.remove("./rc.lock")
        except Exception as err:
            if Path("./rc.lock").is_file():
                os.remove("./rc.lock")
            if Path("./rc2.lock").is_file():
                os.remove("./rc2.lock")
            inkexception(err, "error in to RC")

def parseRocketChat():
    while 1:
        try:
            time.sleep(1)
            while Path("./rc.lock").is_file():
                time.sleep(1)
            open("./rc2.lock", 'a').close()
            idchann = rocketchatChannelId.get(channel9)
            if idchann and idchann != "":
                fname = "./" + idchann
                file = open(fname,"r")
                pattern = idchann + "\-\-\-\-\-"
                data = re.split(pattern, file.read(), 1)
                file.close()
                if len(data) == 2:
                    file = open(fname,"w")
                    sendToRocketChat(data[0])   
                    file.write(data[1])
                    file.close()
            os.remove("./rc2.lock")
        except Exception as err:
            if Path("./rc.lock").is_file():
                os.remove("./rc.lock")
            if Path("./rc2.lock").is_file():
                os.remove("./rc2.lock")
            inkexception(err, "error in parse rc")

def sendToRocketChat(ircmsg):
    try:
        ircmsg = ircmsg.strip('\n\r')
        regex = re.compile("(\x0F|\x16|\x04|\x03|\x11|\x1E|\x1F|\x1D|\x02)(?:\d{1,2}(?:,\d{1,2})?)?", re.UNICODE)
        if ircmsg.find(" 353 " + botnick) != -1:
            headers = {
                    'X-Auth-Token': rocketchatUserToken ,
                    'X-User-Id': rocketchatUserId,
                    'Content-type' :'application/json; charset=UTF-8',
                }
            with open('users.json') as json_file:
                users = json.load(json_file)
            if ircmsg.find("353 " + botnick + " @ " + channel1 + " :") != -1 or ircmsg.find("353 " + botnick + " = " + channel1 + " :") != -1:
                channel = channel1
            elif ircmsg.find("353 " + botnick + " @ " + channel2 + " :") != -1 or ircmsg.find("353 " + botnick + " = " + channel2 + " :") != -1:
                channel = channel2
            elif ircmsg.find("353 " + botnick + " @ " + channel3 + " :") != -1 or ircmsg.find("353 " + botnick + " = " + channel3 + " :") != -1:
                channel = channel3
            elif ircmsg.find("353 " + botnick + " @ " + channel9 + " :") != -1 or ircmsg.find("353 " + botnick + " = " + channel9 + " :") != -1:
                channel = channel9
            else:
            	logger.error("invalid channel", exc_info = True)
            	return
            users[channel]["channelid"] = rocketchatChannelId.get(channel)
            users[channel]["nicks"] = []
            for userspart in re.split("\n:", ircmsg):
                if userspart.find("353 " + botnick + " @ " + channel + " :") != -1 or userspart.find("353 " + botnick + " = " + channel + " :") != -1:
                    if re.split(channel + " :",userspart)[1].replace(botnick + " ","") != "":
                        users[channel]["nicks"] = users[channel]["nicks"] + re.split(channel + " :",userspart)[1].replace(botnick + " ","").split(" ")
            with open('users.json', 'w') as outfile:
                json.dump(users, outfile)
            return

        if ircmsg.find("QUIT") != -1 or ircmsg.find("PART") != -1 or ircmsg.find("JOIN") != -1:
            names(channel1)
            names(channel2)
            names(channel3)
            names(channel9)
            return

        if ircmsg.find("PRIVMSG") != -1:
            name = ircmsg.split('!',1)[0][1:]
            if "NAMES" in name:
                namearray = name.split(":")
                name = namearray[len(namearray) - 1]
            irclogo   = ircIconBasePath + ("irc-%d.png" % (hash(name) % 14 + 2))
            messageroot = re.split('PRIVMSG',ircmsg,1)[0]
            messages = re.split(re.escape(messageroot) + 'PRIVMSG',ircmsg)
            for message in messages:
                if message != "":
                    onchannel = message.split(':',1)[0].strip()
                    message   = message.split(':',1)[1].split("\r\n",1)[0]
                    message   = regex.sub("", message)
                    roomId    = rocketchatChannelId.get(onchannel)

                    if len(name) < 17:
                        if message.find('Hi ' + botnick) != -1:
                            sendmsg("Hello " + name + "!", onchannel)
                            continue
                        elif name == botnick:
                            continue

                    if message != "":
                        if message.find(botnick) != -1:
                            ircsock.send(bytes("PRIVMSG " + name + " :You should message the user on chat.inkscape.org by using an @ symbol and not talk to me, I'm just the messenger. His name is after <" + botnick + "> \r\n", 'UTF-8'))
                        lastmessage = ""
                        headers = {
                            'X-Auth-Token': rocketchatUserToken ,
                            'X-User-Id': rocketchatUserId,
                            'Content-type' :'application/json; charset=UTF-8',
                        }
                        params = ()
                        try:
                            response = requests.get("https://chat.inkscape.org/api/v1/channels.list", headers=headers, params=params)
                            response.raise_for_status()
                        except requests.HTTPError as e:
                            continue
                        channels = response.json()
                        channelist = channels.get("channels")
                        if channelist is not None:
                            for currentchannel in channelist:
                                if currentchannel.get("_id") == roomId:
                                    if currentchannel.get("lastMessage").get("alias") == name:
                                        lastmessage = currentchannel.get("lastMessage").get("_id")

                        if lastmessage != "":
                            params = ()
                            try:
                                response = requests.get('https://chat.inkscape.org/api/v1/chat.getMessage?msgId=' + lastmessage, headers=headers, params=params)
                                response.raise_for_status()
                            except requests.HTTPError as e:
                                continue
                            messagecontent = response.json()
                            mesagetaken = messagecontent.get("message")
                            if mesagetaken is not None:
                                originalmessage = mesagetaken.get("msg")
                                if originalmessage is not None:
                                    if originalmessage[-2:] == '\r\n':
                                        originalmessage = originalmessage[:-2]
                                    if originalmessage[-1:] == '\n':
                                        originalmessage = originalmessage[:-1]
                                    params = {"roomId": roomId, "msgId": lastmessage, "text": originalmessage + "\r\n" + message}
                                    requests.post('https://chat.inkscape.org/api/v1/chat.update', headers=headers, json=params)
                        else:
                            params = { "roomId": roomId,"alias": name, "text": message, "avatar": irclogo}
                            requests.post('https://chat.inkscape.org/api/v1/chat.postMessage', headers=headers, json=params)
                    if name.lower() == adminName.lower() and message.rstrip() == exitCode:
                        sendmsg("oh...okay. :'(", onchannel)
                        ircsock.send(bytes("QUIT \r\n", 'UTF-8'))
                        return
    except Exception as err:
        inkexception(err, "error in send to rc")

login()
retoken()
from inkchatbotToken  import *
t1 = multiprocessing.Process(target=toIRC)
t1.start()
t2 = multiprocessing.Process(target=parseRocketChat)
t2.start()
toRocketChat()
