function addclasses() {
    if (localStorage.getItem('Meteor.loginToken') != null) {
        $('body').addClass("logged");
    } else {
        $('body').removeClass("logged");
    }
}

var active = true;
$(window).focus(function() {
    active = true;
});

$(window).blur(function() {
    active = false;
});

function getIrcInkUsers(withirc) {
    if (document.hidden || !active) {
        return;
    }
    var isuser =  $("main[data-qa-rc-room=DdSRYnE833uGFna8J]").length > 0;
    var isdevel = $("main[data-qa-rc-room=S8QyyXE5DNPi83KuL]").length > 0;
    var isboard = $("main[data-qa-rc-room=HiMXBhfcnEhRyWydQ]").length > 0;
    if (isdevel || isuser || isboard) {
        $.ajax({
            url: "https://inkscape.org/alpha-nick/",
            dataType: "jsonp",
            jsonpCallback: 'ircInkUsers',
            jsonp: 'callback',
            async: false,
            type: 'GET',
            success: function(data) {
                $.each(["#inkscape-devel","#inkscape-board", "#inkscape"], function(index, value) {
                    if ((isdevel && index != 0) || (isuser && index != 2) || (isboard && index != 1)) {
                        return true;
                    }
                    var userlist = $("<div></div>");
                    var userCounter = $("aside .rcx-css-14owjxa");
                    var out = '';
                    nusers = data[value]["nicks"].length;
                    if (withirc) {
                        userCounter.find(".irccounter").remove();
                        out = '<span class="irccounter">IRC: ';
                        out = out + "<a onclick='alert(ircusers)' >" + nusers;
                        out = out + "</a> </span>";
                        out = out + userCounter.html();
                    }
                    usershowing = $('.messages-container .user[data-username="inkchatbot"]');
                    usernames = [];
                    $.each(data[value]["nicks"], function(i, el) {
                        usernames.push(el.replace("@", ""));
                    })
                    $.each(usershowing, function(i, el) {
                        user = $(el).text().trim().split("@")[0].trim();
                        if (usernames.indexOf(user) == -1) {
                            $(el).removeClass("ircactivo");
                            $(el).addClass("ircinactivo");
                        } else {
                            $(el).removeClass("ircinactivo");
                            $(el).addClass("ircactivo");
                        }
                    })
                    if (withirc) {
                    	users = ""
                        for (var i = 0; i < nusers; i++) {
                            users = users + " " + data[value]["nicks"][i];
                        }
                        userCounter.html(out.replace("onclick='alert(ircusers)'", "onclick='alert(\""+users+"\")'"));
                    }
                })
            }
        })
        $('.rcx-button-group__item').off("mousedown").on("mousedown", function() {
  	    if (!$(this).hasClass("active")) {
              setTimeout(function() {
                getIrcInkUsers(true);
    	    }, 1000);
  	}});
    }
}

$('body').ready(function() {
  $("html").on("mousedown",".js-confirm", function(e){
  var description = $('#file-description');
  if (description.val() == "") {
    description.val("File Uploaded");
  }
})
    setTimeout(function() {
        addclasses();
        getIrcInkUsers(false);
    }, 4000);
});

if (typeof getIrcInkUsersTimeout === 'undefined') {
    getIrcInkUsersTimeout = setInterval(function() { getIrcInkUsers(false); }, 60000);
}

const darkModeDefault = false;

const darkModeSymbol = `<svg id="icon-darkmode" viewBox="0 0 468 468" fill="currentColor">
  <path d="m 415.93171,295.15062 c -0.62337,-3.57691 -2.19121,-6.61587 -4.69035,-9.11878 -5.53903,-5.54184 -11.97185,-6.70506 -19.29651,-3.48866 -19.6561,9.11408 -40.03227,13.66736 -61.11443,13.66736 -26.44751,0 -50.83903,-6.51541 -73.17449,-19.56315 -22.33549,-13.04396 -40.02288,-30.73323 -53.06686,-53.07152 -13.04867,-22.33736 -19.56877,-46.72791 -19.56877,-73.17637 0,-20.54893 4.0679,-40.0717 12.19622,-58.567395 8.13019,-18.494768 19.88046,-34.711019 35.24615,-48.64969 5.89861,-5.537165 7.24112,-11.971842 4.02473,-19.299338 -3.03804,-7.323741 -8.48788,-10.806766 -16.34958,-10.450953 -27.33939,1.071195 -53.2499,7.195122 -77.73154,18.359579 C 117.92559,42.958966 96.883794,57.653388 79.281844,75.883409 61.681771,94.109675 47.742161,115.68846 37.466768,140.61509 c -10.272576,24.92852 -15.411681,51.06246 -15.411681,78.40188 0,27.87734 5.450793,54.50229 16.350502,79.87674 10.902525,25.37443 25.552823,47.26398 43.959342,65.66955 18.404639,18.39994 40.294189,33.05306 65.671449,43.95748 25.37445,10.89688 51.99564,16.34954 79.87017,16.34954 40.0313,0 76.9711,-10.85557 110.83154,-32.56769 33.86232,-21.70928 59.19267,-50.70193 75.9919,-86.97703 1.43079,-3.21356 1.83166,-6.60553 1.20172,-10.17493 z"/>
</svg>`; // moon icon
const lightModeSymbol = `<svg id="icon-lightmode" viewBox="0 0 302.4 302.4" fill="currentColor">
  <path d="m204.8,97.6c-13.60001,-13.6 -32.8,-22.4 -53.60001,-22.4s-40,8.4 -53.6,22.4c-13.6,13.6 -22.4,32.8 -22.4,53.6s8.8,40 22.4,53.59999c13.6,13.60001 32.8,22.40001 53.6,22.40001s40,-8.40001 53.60001,-22.40001c13.59999,-13.59999 22.39999,-32.8 22.39999,-53.59999s-8.39999,-40 -22.39999,-53.6zm-14.40001,92.8c-10,10 -24,16 -39.2,16s-29.2,-6 -39.2,-16s-16,-24 -16,-39.2s6,-29.2 16,-39.2s23.99999,-16 39.2,-16s29.2,6 39.2,16s16,23.99999 16,39.2s-6,29.2 -16,39.2z"/>
  <path d="m292,140.8l-30.79999,0c-5.60001,0 -10.40001,4.8 -10.40001,10.39999c0,5.60001 4.8,10.40001 10.40001,10.40001l30.79999,0c5.60001,0 10.39999,-4.8 10.39999,-10.40001c0,-5.59999 -4.79999,-10.39999 -10.39999,-10.39999z"/>
  <path d="m151.2,250.8c-5.59999,0 -10.39999,4.8 -10.39999,10.40001l0,30.79999c0,5.60001 4.8,10.39999 10.39999,10.39999c5.60001,0 10.40001,-4.79999 10.40001,-10.39999l0,-30.79999c0,-5.60001 -4.8,-10.40001 -10.40001,-10.40001z"/>
  <path d="m258,243.60001l-22,-22c-3.60001,-4 -10.39999,-4 -14.39999,0s-4,10.40001 0,14.40001l22,21.99998c4,4 10.39999,4 14.39999,0s4,-10.39999 0,-14.39999z"/>
  <path d="m151.2,0c-5.59999,0 -10.39999,4.8 -10.39999,10.4l0,30.8c0,5.6 4.8,10.4 10.39999,10.4c5.60001,0 10.40001,-4.8 10.40001,-10.4l0,-30.8c0,-5.6 -4.8,-10.4 -10.40001,-10.4z"/>
  <path d="m258.39999,44.4c-4,-4 -10.40001,-4 -14.40001,0l-22,22c-4,4 -4,10.4 0,14.4c3.60001,4 10.40001,4 14.40001,0l22,-22c4,-4 4,-10.4 0,-14.4z"/>
  <path d="m41.2,140.8l-30.8,0c-5.6,0 -10.4,4.8 -10.4,10.39999s4.4,10.40001 10.4,10.40001l30.8,0c5.6,0 10.4,-4.8 10.4,-10.40001c0,-5.59999 -4.8,-10.39999 -10.4,-10.39999z"/>
  <path d="m80.4,221.60001c-3.6,-4 -10.4,-4 -14.4,0l-22,22c-4,4 -4,10.40001 0,14.39999s10.4,4 14.4,0l22,-21.99998c4,-4.00002 4,-10.40001 0,-14.40001z"/>
  <path d="m80.4,66.4l-22,-22c-4,-4 -10.4,-4 -14.4,0s-4,10.4 0,14.4l22,22c4,4 10.4,4 14.4,0s4,-10.4 0,-14.4z"/>
</svg>`; // sun icon

const darkModeToggleText = {
	'en': 'Toggle Dark Mode',
	'de': 'Dark Mode umschalten',
	'fr': 'Toggle Dark Mode',
	'es': 'Toggle Dark Mode',
	'it': 'Toggle Dark Mode',
	'nl': 'Toggle Dark Mode',
	'pl': 'Toggle Dark Mode',
	'pt': 'Alternar Tema',
	'ru': 'Смена оформления',
	'he': 'מצב לילה'
}[defaultUserLanguage()] || 'Toggle Dark Mode';

const toggleButton = `<button class="rcx-box--animated rcx-box rcx-box--full rcx-button--small-square rcx-button--square rcx-button--small rcx-button--ghost rcx-button rcx-button-group__item  js-button" aria-label="${darkModeToggleText}">D</button>`;

function isDarkModeSet() {
	return localStorage.getItem('dark-mode') === 'true';
}

function getDarkModeIcon() {
	return `<svg class="rc-icon sidebar__toolbar-button-icon sidebar__toolbar-button-icon--darkmode" aria-hidden="true">
    <use xlink:href="#icon-darkmode"></use>
    ${isDarkModeSet() ? lightModeSymbol : darkModeSymbol}
  </svg>`;
}

function toggleDarkMode() {
	document.body.classList.toggle('dark-mode');
	const setting = (!isDarkModeSet()).toString();
	localStorage.setItem('dark-mode', setting);
}

function addDarkModeToggle() {
	const sidebarToolbar = $('.rcx-button-group.rcx-css-vi4iz8');

	// wait for the sidebar toolbar to be visible
	// this will also be false if the toolbar doesn't exist yet
	if(!sidebarToolbar.is(':visible')) {
		setTimeout(addDarkModeToggle, 250);
		return;
	}

	var darkModeButton = $(`.js-button[aria-label="${darkModeToggleText}"]`);

	// do nothing if button is already on the screen
	if (darkModeButton.is(':visible')) {
		return;
	}

	darkModeButton = $(toggleButton).prependTo(sidebarToolbar);
	darkModeButton.html(getDarkModeIcon());

	darkModeButton.on('click', function() {
		toggleDarkMode();
		darkModeButton.html(getDarkModeIcon());
		$(this).blur();
	});
}

if (darkModeDefault) {
	if (localStorage.getItem("dark-mode") === null) {
		localStorage.setItem('dark-mode', 'true');
	}
}

$(addDarkModeToggle);

// Apply dark mode immediately if it's been set previously
if(localStorage.getItem('dark-mode') === 'true') {
	document.body.classList.add('dark-mode');
}
